//
//  Category.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name;

- (id) initWithId:(NSString *) id andName:(NSString *) name;
- (id) initWithJsonString:(NSString *) jsonString;

@end
