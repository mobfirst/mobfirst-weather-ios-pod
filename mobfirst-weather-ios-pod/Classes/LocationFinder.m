//
//  LocationFinder.m
//  WeatherLibrary
//
//  Created by Diego Merks on 23/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "LocationFinder.h"

@implementation LocationFinder

- (id) init {
    
    if(self = [super init]) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.delegate = self;
        
        if (([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]))
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    
    return self;
}

- (void) getLastKnownLocationWithCompletionHandler:(void(^)(CLLocation * location)) completionHandler {
    
    self.completionHandler = completionHandler;
    self.completionHandlerTriggered = NO;
    
    [self.locationManager startUpdatingLocation];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [self.locationManager stopUpdatingLocation];
    if(![self completionHandlerTriggered]) {
        
        self.completionHandler(nil);
        self.completionHandlerTriggered = YES;
    }
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    [self.locationManager stopUpdatingLocation];
    if(![self completionHandlerTriggered]) {
        
        self.completionHandler([locations lastObject]);
        self.completionHandlerTriggered = YES;
    }
}

@end
