//
//  WeatherMapper.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "WeatherMapper.h"

@implementation WeatherMapper

+ (NSString *) mapFromWeather:(NSString *) weather {
    
    for(int i = 0; i < [apiWeathers count]; i++) {
        for(int j = 0; j < [apiWeathers[i] count]; j++) {
            
            if([weather caseInsensitiveCompare:apiWeathers[i][j]] == NSOrderedSame) {
                return mappedWeathers[i];
            }
        }
    }
    
    return nil;
}

@end
