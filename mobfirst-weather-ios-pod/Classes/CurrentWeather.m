//
//  CurrentWeather.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "CurrentWeather.h"
#import "Constants.h"

@implementation CurrentWeather

- (id) initWithTemp:(double) temp feelsLike:(double) feelsLike andCondition:(Condition *) condition {
    
    if(self = [super init]) {
        
        self.temp = temp;
        self.feelsLike = feelsLike;
        self.condition = condition;
    }
    
    return self;
}
- (id) initWithJsonString:(NSString *) jsonString {
    
    NSError * error;
    
    if(self = [super init]) {
        
        NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(!jsonDictionary) return self;
        if(error) return self;
        
        self.temp = [[jsonDictionary objectForKey:CURRENT_TEMP] doubleValue];
        self.feelsLike = [[jsonDictionary objectForKey:CURRENT_FEELS_LIKE] doubleValue];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[jsonDictionary objectForKey:CURRENT_CONDITION] options:NSJSONWritingPrettyPrinted error:&error];
        
        self.condition = [[Condition alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    }
    
    return self;
}

@end
