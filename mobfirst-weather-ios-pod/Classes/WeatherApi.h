//
//  WeatherApi.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentWeather.h"

@interface WeatherApi : NSObject

@property (nonatomic, strong) NSString * apiKey;

- (id) initWithApiKey:(NSString *) apiKey;
- (void) getCurrentWeatherWithLatitude:(double) latitude longitude:(double) longitude andCompletionHandler:(void(^)(CurrentWeather * currentWeather)) completionHandler;

@end
