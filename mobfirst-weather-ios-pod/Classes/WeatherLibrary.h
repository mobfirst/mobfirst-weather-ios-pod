//
//  WeatherLibrary.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Category.h"
#import "CurrentWeather.h"
#import "TemperatureMap.h"
#import "LocationFinder.h"
#import "WeatherApi.h"

@interface WeatherLibrary : NSObject

@property (nonatomic, strong) NSString * apiKey;
@property (nonatomic, strong) NSString * temperatureMapJsonUrl;

@property (nonatomic, strong) LocationFinder * locationFinder;
@property (nonatomic, strong) WeatherApi * weatherApi;

- (id) initWithApiKey:(NSString *) apiKey andTemperatureMapJsonUrl:(NSString *) temperatureMapJsonUrl;
- (void) getCategoryUsingFeelsLikeTemp:(BOOL) useFeelsLikeTemp andCompletionHandler:(void(^)(Category * category)) completionHandler;
- (Category *) getCategoryFromCurrentWeather:(CurrentWeather *) currentWeather usingTemperatureMap:(TemperatureMap *) temperatureMap andFeelsLikeTemp:(BOOL) useFeelsLikeTemp;

@end
