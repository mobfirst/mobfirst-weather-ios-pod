//
//  Constants.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define URL_BASE @"https://api.apixu.com/v1"
#define URL_CURRENT @"/current.json"

#define URL_PARAMETER_API_KEY @"key"
#define URL_PARAMETER_QUERY @"q"

#define CONDITION_TEXT @"text"
#define CONDITION_CODE @"code"

#define CURRENT_TEMP @"temp_c"
#define CURRENT_FEELS_LIKE @"feelslike_c"
#define CURRENT_CONDITION @"condition"

#define CURRENT @"current"

#define CATEGORY_ID @"Id"
#define CATEGORY_NAME @"Name"

#define LOW_TEMPERATURE @"LowTemperature"
#define HIGH_TEMPERATURE @"HighTemperature"
#define WEATHER @"Weather"
#define CATEGORY @"Category"

#endif /* Constants_h */


