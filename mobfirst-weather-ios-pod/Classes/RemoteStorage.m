//
//  RemoteStorage.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "RemoteStorage.h"
#import "WeatherLibraryHttp.h"

@implementation RemoteStorage

+ (void) getTemperatureMapFromUrl:(NSString *) url andCompletionHandler:(void(^)(TemperatureMap * temperatureMap)) completionHandler {
    
    [WeatherLibraryHttp doGetFromUrl:url withHeaders:nil parameters:nil andCompletionHandler:^(NSData *data, NSURLResponse *response) {
        
        TemperatureMap * temperatureMap = [[TemperatureMap alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
        completionHandler(temperatureMap);
    }];
}

@end
