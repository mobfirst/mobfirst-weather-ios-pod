//
//  WeatherApi.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "WeatherApi.h"
#import "Constants.h"
#import "QueryBuilder.h"
#import "WeatherLibraryHttp.h"

@implementation WeatherApi

- (id) initWithApiKey:(NSString *) apiKey {
    
    if(self = [super init]) {
        self.apiKey = apiKey;
    }
    
    return self;
}

- (void) getCurrentWeatherWithLatitude:(double) latitude longitude:(double) longitude andCompletionHandler:(void(^)(CurrentWeather * currentWeather)) completionHandler {
    
    NSMutableDictionary * parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[self apiKey] forKey:URL_PARAMETER_API_KEY];
    [parameters setObject:[QueryBuilder buildWithLatitude:latitude andLongitude:longitude] forKey:URL_PARAMETER_QUERY];
    
    [WeatherLibraryHttp doGetFromUrl:[NSString stringWithFormat:@"%@%@", URL_BASE, URL_CURRENT] withHeaders:nil parameters:parameters andCompletionHandler:^(NSData *data, NSURLResponse *response) {
        
        NSError * error;
        NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if(jsonDictionary && !error) {
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[jsonDictionary objectForKey:CURRENT] options:NSJSONWritingPrettyPrinted error:&error];
            completionHandler([[CurrentWeather alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]);
        }
        else {
            completionHandler(nil);
        }
    }];
}

@end
