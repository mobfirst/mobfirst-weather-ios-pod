//
//  TemperatureMap.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TemperatureMap : NSObject

@property (nonatomic, strong) NSMutableArray * temperatureObjects;

- (id) initWithTemperatureObjects:(NSMutableArray *) temperatureObjects;
- (id) initWithJsonString:(NSString *) jsonString;

@end
