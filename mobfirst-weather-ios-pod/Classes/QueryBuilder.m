//
//  QueryBuilder.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "QueryBuilder.h"

@implementation QueryBuilder

+ (NSString *) buildWithLatitude:(double) latitude andLongitude:(double) longitude {
    
    return [NSString stringWithFormat:@"%.2lf,%.2lf", latitude, longitude];
}

@end
