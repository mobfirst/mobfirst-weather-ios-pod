//
//  TemperatureMap.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "TemperatureMap.h"
#import "TemperatureObject.h"

@implementation TemperatureMap

- (id) initWithTemperatureObjects:(NSMutableArray *) temperatureObjects {
    
    if(self = [super init]) {
        self.temperatureObjects = temperatureObjects;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    NSError * error;
    
    if(self = [super init]) {
        
        NSArray * jsonArray = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        self.temperatureObjects = [[NSMutableArray alloc] init];
        
        for(NSDictionary * item in jsonArray) {
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:item options:NSJSONWritingPrettyPrinted error:&error];
            [[self temperatureObjects] addObject:[[TemperatureObject alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
    }
    
    return self;
}

@end
