//
//  QueryBuilder.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QueryBuilder : NSObject

+ (NSString *) buildWithLatitude:(double) latitude andLongitude:(double) longitude;

@end
