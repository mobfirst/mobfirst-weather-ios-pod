//
//  Condition.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Condition.h"
#import "Constants.h"

@implementation Condition

- (id) initWithText:(NSString *) text andCode:(long) code {
    
    if(self = [super init]) {
        
        self.text = text;
        self.code = code;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    NSError * error;
    
    if(self = [super init]) {
        
        NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(!jsonDictionary) return self;
        if(error) return self;
        
        self.text = [jsonDictionary objectForKey:CONDITION_TEXT];
        self.code = [[jsonDictionary objectForKey:CONDITION_CODE] longValue];
    }
    
    return self;
}

@end
