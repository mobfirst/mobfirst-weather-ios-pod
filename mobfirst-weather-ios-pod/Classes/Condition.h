//
//  Condition.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Condition : NSObject

@property (nonatomic, strong) NSString * text;
@property (nonatomic) long code;

- (id) initWithText:(NSString *) text andCode:(long) code;
- (id) initWithJsonString:(NSString *) jsonString;

@end
