//
//  LocationFinder.h
//  WeatherLibrary
//
//  Created by Diego Merks on 23/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationFinder : NSObject<CLLocationManagerDelegate>

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, copy) void (^completionHandler)(CLLocation * location);
@property (nonatomic, assign) BOOL completionHandlerTriggered;

- (id) init;
- (void) getLastKnownLocationWithCompletionHandler:(void(^)(CLLocation * location)) completionHandler;

@end
