//
//  RemoteStorage.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TemperatureMap.h"

@interface RemoteStorage : NSObject

+ (void) getTemperatureMapFromUrl:(NSString *) url andCompletionHandler:(void(^)(TemperatureMap * temperatureMap)) completionHandler;

@end
