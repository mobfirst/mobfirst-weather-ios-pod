//
//  Category.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Category.h"
#import "Constants.h"

@implementation Category

- (id) initWithId:(NSString *) id andName:(NSString *) name {
    
    if(self = [super init]) {
        
        self.id = id;
        self.name = name;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    NSError * error;
    
    if(self = [super init]) {
        
        NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(!jsonDictionary) return self;
        if(error) return self;
        
        self.id = [jsonDictionary objectForKey:CATEGORY_ID];
        self.name = [jsonDictionary objectForKey:CATEGORY_NAME];
    }
    
    return self;
}

@end
