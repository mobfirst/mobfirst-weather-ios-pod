//
//  TemperatureObject.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "TemperatureObject.h"
#import "Constants.h"

@implementation TemperatureObject

- (id) initWithLowTemp:(double) lowTemp highTemp:(double) highTemp weather:(NSString *) weather andCategory:(Category *) category {
    
    if(self = [super init]) {
        
        self.lowTemperature = lowTemp;
        self.highTemperature = highTemp;
        self.weather = weather;
        self.category = category;
    }
    
    return self;
}
- (id) initWithJsonString:(NSString *) jsonString {
    
    NSError * error;
    
    if(self = [super init]) {
        
        NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(!jsonDictionary) return self;
        if(error) return self;
        
        self.lowTemperature = [[jsonDictionary objectForKey:LOW_TEMPERATURE] doubleValue];
        self.highTemperature = [[jsonDictionary objectForKey:HIGH_TEMPERATURE] doubleValue];
        self.weather = [jsonDictionary objectForKey:WEATHER];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[jsonDictionary objectForKey:CATEGORY] options:NSJSONWritingPrettyPrinted error:&error];
        
        self.category = [[Category alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    }
    
    return self;
}

@end
