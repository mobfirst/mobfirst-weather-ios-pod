//
//  WeatherLibraryHttp.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TIMEOUT_INTERVAL 5.0

@interface WeatherLibraryHttp : NSObject

+ (void) doGetFromUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler;

@end
