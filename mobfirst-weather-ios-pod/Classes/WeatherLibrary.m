//
//  WeatherLibrary.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "WeatherLibrary.h"
#import "TemperatureObject.h"
#import "WeatherMapper.h"
#import "Condition.h"
#import "RemoteStorage.h"

@implementation WeatherLibrary

- (id) initWithApiKey:(NSString *) apiKey andTemperatureMapJsonUrl:(NSString *) temperatureMapJsonUrl {
    
    if(self = [super init]) {
        
        self.apiKey = apiKey;
        self.temperatureMapJsonUrl = temperatureMapJsonUrl;
    }
    
    return self;
}

- (void) getCategoryUsingFeelsLikeTemp:(BOOL) useFeelsLikeTemp andCompletionHandler:(void(^)(Category * category)) completionHandler {
    
    self.locationFinder = [[LocationFinder alloc] init];
    [self.locationFinder getLastKnownLocationWithCompletionHandler:^(CLLocation * location) {
        
        if(location) {
            
            [RemoteStorage getTemperatureMapFromUrl:[self temperatureMapJsonUrl] andCompletionHandler:^(TemperatureMap *temperatureMap) {
                
                if(temperatureMap) {
                    
                    self.weatherApi = [[WeatherApi alloc] initWithApiKey:[self apiKey]];
                    [self.weatherApi getCurrentWeatherWithLatitude:[location coordinate].latitude longitude:[location coordinate].longitude andCompletionHandler:^(CurrentWeather *currentWeather) {
                        
                        if(currentWeather) {
                            
                            Category * category = [self getCategoryFromCurrentWeather:currentWeather usingTemperatureMap:temperatureMap andFeelsLikeTemp:useFeelsLikeTemp];
                            completionHandler(category);
                        }
                        else {
                            completionHandler(nil);
                        }
                    }];
                }
                else {
                    completionHandler(nil);
                }
            }];
        }
        else {
            completionHandler(nil);
        }
    }];
}

- (Category *) getCategoryFromCurrentWeather:(CurrentWeather *) currentWeather usingTemperatureMap:(TemperatureMap *) temperatureMap andFeelsLikeTemp:(BOOL) useFeelsLikeTemp {
    
    for(TemperatureObject * temperatureObject in [temperatureMap temperatureObjects]) {
        
        if(useFeelsLikeTemp) {
            if([currentWeather feelsLike] > [temperatureObject highTemperature]) continue;
            if([currentWeather feelsLike] < [temperatureObject lowTemperature]) continue;
        }
        else {
            if([currentWeather temp] > [temperatureObject highTemperature]) continue;
            if([currentWeather temp] < [temperatureObject lowTemperature]) continue;
        }
        
        if([[temperatureObject weather] caseInsensitiveCompare:[WeatherMapper mapFromWeather:[[currentWeather condition] text]]] == NSOrderedSame) {
            return [temperatureObject category];
        }
    }
    
    return nil;
}

@end
