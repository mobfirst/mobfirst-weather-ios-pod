//
//  CurrentWeather.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Condition.h"

@interface CurrentWeather : NSObject

@property (nonatomic) double temp;
@property (nonatomic) double feelsLike;
@property (nonatomic, strong) Condition * condition;

- (id) initWithTemp:(double) temp feelsLike:(double) feelsLike andCondition:(Condition *) condition;
- (id) initWithJsonString:(NSString *) jsonString;

@end
