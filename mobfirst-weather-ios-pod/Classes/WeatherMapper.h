//
//  WeatherMapper.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

#define mappedWeathers @[ @"Sunny", @"Cloudy", @"Overcast", @"Rain", @"Snow" ]
#define apiWeathers @[ @[ @"Sunny", @"Clear" ], @[ @"Partly cloudy", @"Cloudy" ], @[ @"Overcast", @"Mist", @"Fog" ], @[ @"Patchy rain possible", @"Thundery outbreaks possible", @"Patchy sleet possible", @"Patchy light drizzle", @"Light drizzle", @"Patchy light rain", @"Light rain", @"Moderate rain at times", @"Moderate rain", @"Heavy rain at times", @"Heavy rain", @"Light sleet", @"Moderate or heavy sleet", @"Light rain shower", @"Moderate or heavy rain shower", @"Torrential rain shower", @"Light sleet showers", @"Moderate or heavy sleet showers", @"Patchy light rain with thunder", @"Moderate or heavy rain with thunder" ], @[ @"Patchy snow possible", @"Patchy freezing drizzle possible", @"Blowing snow", @"Blizzard", @"Freezing fog", @"Freezing drizzle", @"Heavy freezing drizzle", @"Light freezing rain", @"Moderate or heavy freezing rain", @"Patchy light snow", @"Light snow", @"Patchy moderate snow", @"Moderate snow", @"Patchy heavy snow", @"Heavy snow", @"Ice pellets", @"Light snow showers", @"Moderate or heavy snow showers", @"Light showers of ice pellets", @"Moderate or heavy showers of ice pellets", @"Patchy light snow with thunder", @"Moderate or heavy snow with thunder" ] ]

@interface WeatherMapper : NSObject

+ (NSString *) mapFromWeather:(NSString *) weather;

@end
