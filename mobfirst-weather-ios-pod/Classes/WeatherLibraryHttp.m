//
//  WeatherLibraryHttp.m
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "WeatherLibraryHttp.h"

@implementation WeatherLibraryHttp

+ (void) doGetFromUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler {
    
    NSMutableString * urlWithParameters = [[NSMutableString alloc] initWithString:stringUrl];
    BOOL firstParameter = YES;
    
    if(parameters) {
        for(NSString *key in [parameters allKeys]) {
            
            [urlWithParameters appendString:[NSString stringWithFormat:firstParameter ? @"?%@=%@" : @"&%@=%@", key, [parameters objectForKey:key]]];
            firstParameter = NO;
        }
    }
    
    NSURL * url = [NSURL URLWithString:[urlWithParameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    [request setHTTPMethod:@"GET"];
    
    for(NSString *key in [headers allKeys]) {
        [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        completionHandler(data, response);
    }] resume];
}

@end
