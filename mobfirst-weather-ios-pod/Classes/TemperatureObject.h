//
//  TemperatureObject.h
//  WeatherLibrary
//
//  Created by Diego Merks on 24/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Category.h"

@interface TemperatureObject : NSObject

@property (nonatomic) double lowTemperature;
@property (nonatomic) double highTemperature;
@property (nonatomic, strong) NSString * weather;
@property (nonatomic, strong) Category * category;

- (id) initWithLowTemp:(double) lowTemp highTemp:(double) highTemp weather:(NSString *) weather andCategory:(Category *) category;
- (id) initWithJsonString:(NSString *) jsonString;

@end
