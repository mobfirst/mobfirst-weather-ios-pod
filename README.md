# mobfirst-weather-ios-pod

[![CI Status](http://img.shields.io/travis/Diego Merks/mobfirst-weather-ios-pod.svg?style=flat)](https://travis-ci.org/Diego Merks/mobfirst-weather-ios-pod)
[![Version](https://img.shields.io/cocoapods/v/mobfirst-weather-ios-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-weather-ios-pod)
[![License](https://img.shields.io/cocoapods/l/mobfirst-weather-ios-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-weather-ios-pod)
[![Platform](https://img.shields.io/cocoapods/p/mobfirst-weather-ios-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-weather-ios-pod)

### Usage

1 - The WeatherLibrary constructor takes two arguments, in the following order:

1.1 - The APIXU API key   
1.2 - The URL of the json file that contains the temperature map for the product categories   

2 - The getCategory method from the WeatherLibrary takes two arguments, in the following order:

2.1 - A boolean indicating if the temperature comparisons should be done using the real temperature or the felt air temperature (YES = felt temperature, NO = real temperature)   
2.2 - The complation handler block   

3 - Possible weathers for the json file: Sunny, Cloudy, Overcast, Rain, Snow

### Example

```objc
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.weatherLibrary = [[WeatherLibrary alloc] initWithApiKey:@"696ccbc884ed4a55a50192742172108" andTemperatureMapJsonUrl:@"https://s3.amazonaws.com/files.mobfirst.com/Weather+library+test/temperature+map+2.json"];
    
    [self.weatherLibrary getCategoryUsingFeelsLikeTemp:NO andCompletionHandler:^(Category *category) {
        
        if(category) {
            // Success
        }
        else {
            // Error
        }
    }];
}
```

## Requirements

## Installation

mobfirst-weather-ios-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "mobfirst-weather-ios-pod"
```

## Author

Diego Merks, merks@mobfirst.com

## License

mobfirst-weather-ios-pod is available under the MIT license. See the LICENSE file for more info.
